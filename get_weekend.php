
<?php 
    class GetWeekend
    {    
        public function sql_test()
        {

            $months = array('Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

            $results = $this->weekends($months);                                                                                                                                                                                                              
            
            return response()->json($results);
        }

        public function weekends($mths){
            // so we only have future weekends need to know 'now'.
            $now = array('y'=>date('Y'),'m'=>date('m'));
            $wkends=[];
            $d=[];
            $m=[];
            $y=[];
            $maxDays=[];
            $saturday=[];
            $friday=[];
            foreach($mths as $k => $v){
                // is the requested month in the past for this year?
                $m = date('m',strtotime($now['y'].'-'.$v.'-01'));
                // if so, request next year
                $y = $now['y'];
                // What day is the first of the month, 1 for Monday, 7 for friday
                $d = date('N',strtotime($y.'-'.$m.'-1'));
                $saturday = (7 - $d);
                $friday   = ($saturday - 1);
                // And just how many days in the month
                $maxDays = date('t',strtotime($y.'-'.$m.'-1'));
                do {
                    // edge case when friday is 1st of month
                    if ($saturday == 0){
                        // $wkends[$v][] = $y.'-'.$m.'-1 (Fri)';
                    } else {
                        // $wkends[$v][] = $y.'-'.$m.'-'.$saturday.' (Sat)';
                        if ($friday <= $maxDays){
                            $wkends[$v][] = $y.'-'.$m.'-'.$friday.' (Fri)';
                        }
                    }
                    $saturday = ($saturday + 7);
                    $friday = ($friday + 7);
                } while ($saturday <= $maxDays);
            }
            return $wkends;
        }
}